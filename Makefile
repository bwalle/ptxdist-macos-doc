PDFLATEX = pdflatex
BASENAME = ptxdist_mac
TMPEXTEN = aux log out toc

# Make sure that we run LaTeX twice so that all references are in place
all:
	$(MAKE) clean_pdf
	$(MAKE) pdf
	@latex_count=3 ; \
	while egrep -s 'Rerun (LaTeX|to get cross-references right)' ${BASENAME}.log && [ $$latex_count -gt 0 ] ;\
	do \
		echo "Rerunning $(PDF_TOOL)...." ;\
		$(MAKE) clean_pdf ; \
		$(MAKE) pdf ; \
		latex_count=`expr $$latex_count - 1` ;\
	done


# Delete only the PDF file, not the index files
clean_pdf:
	rm -f ${BASENAME}.pdf

# Build only the PDF, used by vim-latex.
pdf: ${BASENAME}.pdf

# Opens the Mac OS default PDF viewer
view: ${BASENAME}.pdf
	@open $<

# Delete all generated files, including the PDF
clean: clean_pdf
	rm -f $(addprefix $(BASENAME).,$(TMPEXTEN))

# Teach make how to build a PDF from a TeX file
%.pdf: %.tex
	$(PDFLATEX) $<

.PHONY: clean view clean_pdf
