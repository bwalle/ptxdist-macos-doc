\documentclass[DIV12,toc=bibliography,parskip=half]{scrreprt}
\usepackage{scrhack}
\usepackage[UKenglish]{babel}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage[utf8]{inputenc}
\usepackage{lmodern,pifont}
\usepackage{graphicx}

\newenvironment{warning}
  { \begin{list}{}{\leftmargin=1cm
                   \labelwidth=\leftmargin}\item[\Large\ding{43}~~]}
  {\end{list}}

%\renewcommand{\familydefault}{\sfdefault}

\usepackage{xcolor}
\definecolor{susepflaume}{rgb}{0,0,0.45}
\definecolor{listinggray}{gray}{0.95}

\pagestyle{headings}
\usepackage{libertine}

%%%%% hyperlinks

\usepackage[
        pdftitle={Using PTXdist on Mac OS},%
        pdfsubject={},%
        pdfstartview=FitH,%
        pdfkeywords={},%
        bookmarks=true,%
        colorlinks=true,%
        linkcolor=susepflaume,%
        citecolor=susepflaume,%
        filecolor=susepflaume,%
        urlcolor=susepflaume,%
        pdfauthor={Bernhard Walle}]{hyperref}
%%%%%%
\urlstyle{sf}

\usepackage{listings}
\lstset{%
        backgroundcolor=\color{listinggray},
        rulecolor=\color{listinggray},
        fillcolor=\color{listinggray},
        basicstyle=\ttfamily\small,%
        escapechar={},
        captionpos=b,
        columns=fullflexible,
        tabsize=4,
        showstringspaces=false,
        keepspaces=true,
        frame=tb,
        xleftmargin=8mm,
        xrightmargin=0mm,
        framexleftmargin=8mm,
        framextopmargin=1mm,
        framexbottommargin=1mm,
        numbers=left,
        numberstyle=\tiny\sffamily,
        stepnumber=1,
        firstnumber=1,
        aboveskip=\bigskipamount,
        belowskip=\bigskipamount
}
\lstdefinestyle{inline}{%
        frame=none,
        numbers=none,
        xleftmargin=5mm,
        framexleftmargin=1mm,
        xrightmargin=5mm,
        framexrightmargin=1mm,
        belowskip=\smallskipamount,
}
\lstdefinestyle{smallinline}{%
        frame=none,
        numbers=none,
        xleftmargin=5mm,
        framexleftmargin=1mm,
        xrightmargin=5mm,
        framexrightmargin=1mm,
        belowskip=\smallskipamount,
        basicstyle=\ttfamily\tiny,%
}
\lstdefinestyle{middleinline}{%
        frame=none,
        numbers=none,
        xleftmargin=5mm,
        framexleftmargin=1mm,
        xrightmargin=5mm,
        framexrightmargin=1mm,
        belowskip=\smallskipamount,
        basicstyle=\ttfamily\scriptsize,%
}

%%% Commands simplifying the life
\newcommand{\macos}{Mac~OS}

\begin{document}

\title{Using PTXdist on \macos{}}
\subtitle{based on PTXdist 2012.04}
\author{Bernhard Walle\footnote{\url{bernhard@bwalle.de}}}
\date{2012-04-20}
\maketitle
\tableofcontents

\newpage
\chapter{Motivation}

\emph{PTXdist} (\url{http://www.ptxdist.org}) is a great way to build Embedded
Linux systems.  It downloads all required components, configures them for
cross-compilation and finally builds a target image and/or target packages.
In addition, it provides an easy way to build a cross-toolchain for most
common processors. Read \cite{PTXdistInstallation} for a description how to
use \emph{PTXdist.}

While a Linux based host is normally the platform of choice for Embedded Linux development,
it is not absolutely \emph{necessary} to use Linux for this. In commercial
environments, it's quite common to use Microsoft Windows. Commerical Linux
distributions such as \emph{MontaVista Linux} provide Windows IDEs. Under the
hood, Cygwin is used for much of the work.

Since \macos{} is a BSD-based Unix, there's no need for such an ``emulation
layer''. However, since BSD is not GNU, and \macos{} doesn't use the ELF binary
format but Mach-O, things are not as easy as it may seem.

The reader may ask why I use \macos{} to build Linux systems. Even on a Mac
computer, Linux can be installed for dual booting. However, I don't like dual
boot. You have to install, maintain and configure everything twice, it takes a
lot of time and even disk space. The more comfortable approach would be to use
some virtualization and install Linux there, which is what I do in parallel.
So the answer just may be just ``because I can''.

But before starting with the manual I would like to thank the Pengutronix team
for providing \emph{PTXdist} to the community and keeping it up-to-date.
Especially I would like to thank \textsc{Michael Olbrich} for reviewing and
accepting my patches and \textsc{Jürgen Beisert} for the good documentation
and for useful comments to this document. Also I want to thank \textsc{Andreas Bießmann} who
developed some of the \macos{}-related patches especially for the toolchain
and who gave valuable feedback on this document.

\chapter{Basics}

This guide should make it a bit easier if you decide to use \emph{PTXdist} on
\macos{} the first time. You should read \cite{PTXdistBSP} and
\cite{PTXdistInstallation} before reading this documentation. I would also
recommend you to install and use \emph{PTXdist} on Linux first and get started
with it. Using Linux in VirtualBox\footnote{\url{http://www.virtualbox.org}}
is probably the easiest method if you don't have a second computer that runs
Linux.

\macos{} has a fancy GUI and you probably never ever need to use the command
line if you do not want. However, \emph{PTXdist} is based on the command line,
you need a Terminal to use it. You can use \emph{Terminal.app} from Apple, but
I would recommend \emph{iTerm 2}\footnote{\url{http://www.iterm2.com}} as
terminal emulator.

\section{Getting OpenSource Software on \macos{}}

While \macos{} comes with quite a lot (compared to Windows) command line tools,
this is not sufficient to do Linux development. The set of commands available
can be compared to a minimal installation of a Linux distribution.

Instead of doing everything manually, people already built systems which makes
it easy to install additional (Unix-based) software on \macos{}:

\begin{itemize}
  \item \textbf{MacPorts} is based on the BSD ports system. It installs
    everything from source and has quite a huge number of packages\footnote{sometimes
    there are even packages that Linux distributions like openSUSE don't have
    in their repositories}. It can be downloaded from \url{http://www.macports.org}
    and you need to install Xcode from Apple including the command line
    utilities before. Its documentation \cite{MacPortsGuide} contains
    everything you need to get started.


  \item \textbf{Fink} (\url{http://www.finkproject.org}) uses the package
    management tools and the package format from Debian.  Contrary to
    MacPorts, it has both binaries and can be built from source (but as far as
    I know, the binaries are less up-to-date compared the source packages).

  \item \textbf{Homebrew} (\url{http://mxcl.github.com/homebrew/}) is an
    alternative to \emph{MacPorts}.
\end{itemize}

As you may have guessed, I personally use \emph{MacPorts}. When it matters,
the documentation focusses on \emph{MacPorts} for simplicity. However,
\emph{PTXdist} is known to be work also with the tools installed with \emph{Fink}.
In fact, it's not required to use any of them. Just install the required tools
manually.


\section{Preparing the Hard Disk}

Unfortunately, the default file system of \macos{} is \emph{case insensitive}
(but \emph{case preserving}). So if a file \textsf{Foo} exists and you create
a file \textsf{foo,} then instead of creating a new file, the already existing
\textsf{Foo} is used.

As \emph{PTXdist} is a Linux build system, it assumes a case sensitive file
system. Luckily, the \macos{} file system \emph{HFS+} can also be used in a case
sensitive mode. Whenever you create a file system with \emph{Disk Utility,}
you can choose the case sensitive variant. So there are various ways to
solve the problem with the case insensitive file system:

\begin{enumerate}
  \item Re-install \macos{} on a case sensitive HFS+ file system. \newline
    This option is not recommended as some applications assume a case
    insensitive file system and will not run on a case sensitive file system.
    A famous example are the Adobe applications.

  \item Use a network file system (NFS in that case). \newline
    This is just slow and not recommended at all.

  \item Use an external disk and format it using a case sensitive file system.\footnote{For
    simplicity I would recommend HFS+.} \newline
    If you want to use \emph{PTXdist} only on this external disk, everything
    is fine with that solution. But note that the speed of external disks
    (except maybe ones connected with \emph{Thunderbolt}\footnote{%
    \url{http://en.wikipedia.org/wiki/Thunderbolt_(interface)}}) is normally
    lower compared to internal disks.

  \item Use a case sensitive data partition on your internal disk. \newline
    As \macos{} partitions can be resized online, this works quite well. Only
    if you have installed Windows or Linux on your Mac using \emph{BootCamp,}
    you may experience problems. But if \macos{} is the only operating system,
    this works fine.

  \item Use a disk image, a so-called \emph{sparse bundle}. \newline
    This is just a file with a file system inside it. In Linux terms, the
    file is then ``loopback mounted''. Section~\ref{sec:sparsebundle}
    describes this approach.
\end{enumerate}

It's not required to install \emph{PTXdist} itself in a case sensitive
partition. Only the projects that are built need to be located there. When
building a \emph{PTXdist} project, it automatically checks if the file system
is case insensitive and rejects the build in that case\footnote{The check is
performed in the \texttt{check\_dirs} function in the \texttt{ptxdist} main
program.}

\begin{warning}
You may want to disable \emph{Spotlight} for this image as it probably doesn't
contain anything useful for indexing and it only makes your computer slower.
To do so, execute \texttt{mdutil -i off \emph{/Volumes/Development}} on the
attached volume. Just replace \texttt{\emph{Development}} with the real name
of the volume.

To disable indexing only for some directories, you can add it to the list of
excluded directories in the \textsf{System Settings} $\rightarrow$
\textsf{Spotlight} $\rightarrow$ \textsf{Privacy} tab.
\end{warning}

\subsection{Creating and Using a \emph{Sparse Bundle}}
\label{sec:sparsebundle}

You can create and maintain disk images using the \emph{Disk Utility} which
comes with \macos{}. However, in my opinion it's much easier on the command line
using \emph{hdiutil(1)}. In order to create a new disk image, just execute

\begin{lstlisting}[style=inline]
% hdiutil create -size 100g \
    -fs "Case-sensitive Journaled HFS+" \
    -volname "Development" \
    devel.sparsebundle
\end{lstlisting}

Of course you can choose another size than 100~Gigabytes as I did in the
example above. As the volume name is used as mount point in \textsf{/Volumes,}
it's recommended to not use any whitespace inside.

After the image is created, you can actually mount it using

\begin{lstlisting}[style=inline]
% hdiutil attach devel.sparsebundle
\end{lstlisting}

Now you should have an empty file system inside \textsf{/Volume/Development}
which you can use to build Embedded Linux Systems. To detach from it, i.\,e.
to unmount it, use

\begin{lstlisting}[style=inline]
% hdiutil detach /Volumes/Development
\end{lstlisting}

After detaching, you can even resize the image using the \texttt{hdiutil
resize} command.

\chapter{Installing PTXdist}

\section{Requirements}

\subsection{\macos{}}

The documentation focusses on \macos{} 10.7 (Lion). The presented flow is also
reported to apply to \macos{} 10.6 (Snow Leopard). Users of \macos{} 10.6 may
safely ignore section \ref{sec:hostcompiler}.

\subsection{Host compiler}
\label{sec:hostcompiler}

PTXdist requires a working host compiler. Apple bundles the compiler with the IDE.
The guide has been tested with following version of LLVM-GCC which comes with
Xcode~4.3.2.

\begin{lstlisting}[style=middleinline]
% gcc --version
i686-apple-darwin11-llvm-gcc-4.2 (GCC) 4.2.1 (Based on Apple Inc. build 5658) \
    (LLVM build 2336.9.00)
Copyright (C) 2007 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
\end{lstlisting}

Normally Xcode is installed via the \emph{AppStore}. Alternatively, you can
download Xcode (including older versions) from the \emph{Apple Developer Website}
(\url{https://developer.apple.com/downloads/index.action}). You have to
register there (an Apple ID is not enough), but it's free.

To get the command line tools, you need to start \emph{Xcode}. You can cancel
the startup dialog. Then select in the menu \emph{Xcode} $\rightarrow$
\emph{Preferences} $\rightarrow$ \emph{Downloads} $\rightarrow$
\emph{Components} $\rightarrow$ \emph{Command Line Tools} $\rightarrow$
\emph{Install}. Figure~\ref{fig:xcode_commandline_install} shows the dialog to
install the tools.

\begin{figure}[htb]
  \begin{center}
    \includegraphics[width=12cm]{xcode_commandline_install.png}
  \end{center}
  \caption{Installation of Xcode commandline tools}
  \label{fig:xcode_commandline_install}
\end{figure}

\section{GNU Tools}

\emph{PTXdist} consists of a lot of shell scrips and Makefiles. Making them
``portable'' which means to use only the minimal subset of commands and
options specified by POSIX and maybe the \emph{Single Unix Specification}
would be a nightmare.  Instead, we just install the GNU variants. At a
minimum, you need to install the tools mentioned in table \ref{tab:gnutools}.

\begin{table}[ht]
  \centering
  \begin{tabular}{|lp{85mm}l|}
    \hline
    \textbf{Name}        & \textbf{Description}                                                 & \textbf{MacPorts / Fink} \\
    \hline
    \hline
    GNU awk              & The GNU awk utility.                                                 & \texttt{gawk}             \\
    GNU Coreutils        & Standard programs like \emph{uname(1)}.                              & \texttt{coreutils}        \\
    GNU tar              & The standard tape archiver with more options.                        & \texttt{gnutar / tar}     \\
    GNU Findutils        & \emph{find(1)}, \emph{locate(1)} and \emph{xargs(1)}.                & \texttt{findutils}        \\
    GNU sed              & Command-line stream editor \emph{sed(1)}.                            & \texttt{gsed / sed}       \\
    GNU wget             & Downloads files from the web via HTTP or FTP.                        & \texttt{wget}             \\
    xz                   & \emph{xz(1)} and \emph{unxz(1)} compression and uncompression tools. & \texttt{xz}               \\
    quilt (optional)     & Tool to make working with the patches easier.                        & \texttt{quilt}            \\
    dialog (optional)    & Needs to be installed to use \texttt{ptxdist menu.}                  & \texttt{dialog}           \\
    \hline
  \end{tabular}
  \caption{GNU tools required to be installed to use \emph{PTXdist}}
  \label{tab:gnutools}
\end{table}

Even if you put \textsf{/opt/local/bin} (that's the place where
\emph{MacPorts} installs its binaries) in front of your \texttt{\$PATH},
\emph{MacPorts} doesn't overwrite existing tools. So for example even if
\emph{GNU date} is present, the command \texttt{date} still points to the BSD
variant from Apple. The GNU variant is available as \texttt{gdate}. In
addition, they are provided without the prefix in
\textsf{/opt/local/libexec/gnubin}.

\section{Installation}

Grab a copy of \emph{PTXdist} at \url{http://www.ptxdist.org/software/ptxdist/download/}.
You can also use the GIT tree, but then you need install the Autotools on the
host first --- they are not required when using the tarball because that one
ships with generated files.

First, extract the tarball and change into the newly created directory:

\begin{lstlisting}[style=inline]
% tar xvf ptxdist-2012.04.0.tar.bz2
% cd ptxdist-2012.04.0/
\end{lstlisting}

After that, configure it:

\begin{lstlisting}[style=inline]
% PATH=/opt/local/libexec/gnubin:$PATH \
    ./configure --prefix=/opt/ptxdist
\end{lstlisting}

The prefix \textsf{/opt/ptxdist} is just a proposal, you can use any other
directory of your choice, but it's recommended to keep it separate from
the system (\textsf{/usr}) and from \emph{MacPorts} (\textsf{/opt/local}).

You might have noticed the \texttt{PATH} setting: We use the non-prefixed GNU
tools. But we don't need to export the \texttt{PATH} because \emph{PTXdist}
remembers the location of the tools it has found in the the configuration step:
It puts symlinks in the binary directory of the installation. We will see later.

Now just build the small parts of \emph{PTXdist} that need to be built:

\begin{lstlisting}[style=inline]
% make
\end{lstlisting}

Finally, install the binaries, scripts and recipes:

\begin{lstlisting}[style=inline]
% sudo make install
\end{lstlisting}

Now let's look in \textsf{/opt/ptxdist/lib/ptxdist-2012.04.0/bin}:

\begin{lstlisting}[style=smallinline]
% ls -l /opt/ptxdist/lib/ptxdist-2012.04.0/bin
lrwxr-xr-x  1 root    admin     19  6 Apr 19:42 awk@ -> /opt/local/bin/gawk
lrwxr-xr-x  1 root    admin     31  6 Apr 19:42 chmod@ -> /opt/local/libexec/gnubin/chmod
lrwxr-xr-x  1 root    admin     31  6 Apr 19:42 chown@ -> /opt/local/libexec/gnubin/chown
lrwxr-xr-x  1 root    admin     28  6 Apr 19:42 cp@ -> /opt/local/libexec/gnubin/cp
lrwxrwxr-x  1 bwalle  admin     30  7 Apr 09:00 find@ -> /opt/local/libexec/gnubin/find
lrwxr-xr-x  1 root    admin     33  6 Apr 19:42 install@ -> /opt/local/libexec/gnubin/install
lrwxr-xr-x  1 root    admin     32  6 Apr 19:42 md5sum@ -> /opt/local/libexec/gnubin/md5sum
lrwxr-xr-x  1 root    admin     31  6 Apr 19:42 mkdir@ -> /opt/local/libexec/gnubin/mkdir
lrwxr-xr-x  1 root    admin     31  6 Apr 19:42 mknod@ -> /opt/local/libexec/gnubin/mknod
lrwxr-xr-x  1 root    admin     28  6 Apr 19:42 mv@ -> /opt/local/libexec/gnubin/mv
-rwxrwxr-x  1 root    admin  57707  6 Apr 19:40 ptxdist*
lrwxr-xr-x  1 root    admin     24  6 Apr 19:42 python@ -> /opt/local/bin/python2.7
lrwxr-xr-x  1 root    admin     34  6 Apr 19:42 readlink@ -> /opt/local/libexec/gnubin/readlink
lrwxr-xr-x  1 root    admin     28  6 Apr 19:42 rm@ -> /opt/local/libexec/gnubin/rm
lrwxr-xr-x  1 root    admin     31  6 Apr 19:42 rmdir@ -> /opt/local/libexec/gnubin/rmdir
lrwxr-xr-x  1 root    admin     29  6 Apr 19:42 sed@ -> /opt/local/libexec/gnubin/sed
lrwxr-xr-x  1 root    admin     30  6 Apr 19:42 stat@ -> /opt/local/libexec/gnubin/stat
lrwxr-xr-x  1 root    admin     29  6 Apr 19:42 tar@ -> /opt/local/libexec/gnubin/tar
lrwxr-xr-x  1 root    admin     31  6 Apr 19:42 xargs@ -> /opt/local/libexec/gnubin/xargs
\end{lstlisting}

As you can see, there are a lot of symbolic links in this directory.
\emph{PTXdist} sets the \texttt{\$PATH} to include that directory in the front
just after startup and so the ``right'' tools are used without modification of
the system's \texttt{\$PATH}. Latter would have a lot of side effects, so it
should be avoided.

Since we installed the software to \textsf{/opt/ptxdist}, we create a symlink to
have it in the search path:

\begin{lstlisting}[style=inline]
% sudo ln -s /opt/ptxdist/bin/ptxdist /usr/local/bin
\end{lstlisting}


\section{First setup}

Just after installing \emph{PTXdist}, configure it with

\begin{lstlisting}[style=inline]
ptxdist setup
\end{lstlisting}

Now a setup screen as shown in figure \ref{fig:ptxdist_setup} should appear.

\begin{figure}[htb]
  \begin{center}
    \includegraphics[width=12cm]{ptxdist_setup.png}
  \end{center}
  \caption{Setup screen of \emph{PTXdist}}
  \label{fig:ptxdist_setup}
\end{figure}

PTXdist documentation \cite{PTXdistBSP} explains the various options.

\chapter{Building an OSELAS.Toolchain}

While \emph{PTXdist} can be used with most (cross) toolchains, the easiest
is to use OSELAS.Toolchain
(\url{http://www.pengutronix.de/oselas/toolchain/index_en.html}).

As the current GIT version of the toolchain contains some unreleased fixes, I
recommend using that one\footnote{Since ``current GIT'' is a moving target,
you can use the hash 7f195586c0170ba6ec71c01ab73d465b94d6dbbd.}:

\begin{lstlisting}[style=inline]
% git clone git://git.pengutronix.de/git/OSELAS.Toolchain.git
% cd OSELAS.Toolchain
\end{lstlisting}

In our example we build the toolchain for armv4 to be used with Qemu, but nothing
differs if you select another one.

\begin{lstlisting}[style=inline]
% ptxdist select ptxconfigs/arm-v4t-*.ptxconfig
\end{lstlisting}

Now the recommended way would be to use the version of \emph{PTXdist} for which
the current project was built. However, \emph{PTXdist} 2011.11 doesn't contain
the fixes required for \macos{}. So as a exception, we migrate the project:

\begin{lstlisting}[style=inline]
% ptxdist migrate
\end{lstlisting}

You can run \texttt{git diff} to see what that command changed. If everything
went right, only some version strings should have changed.  Now we can build the
toolchain which takes some hours, even on up-to-date hardware:

\begin{lstlisting}[style=inline]
% ulimit -n 5000
% ptxdist go
\end{lstlisting}

The call to \emph{ulimit(1)} is required at least on my Mac because otherwise
I get the error ``too many open files'' when building glibc, even when I build
with \texttt{{-}-j-intern=1} which disables parallelization. The error results
in the failure to build codepage conversion objects.

Since the toolchain will be installed into \textsf{/opt}, \texttt{sudo} asks you
to enter your password.

After the toolchain build has finished, you should have
\textsf{/opt/OSELAS.Toolchain-2011.11.0/arm-v4t-linux-gnueabi/gcc-4.6.2-glibc-2.14.1-binutils-2.21.1a-kernel-2.6.39-sanitized/bin/arm-v4t-linux-gnueabi-gcc}
and some other files in that directory such as
\textsf{arm-v4t-linux-gnueabi-gdb} for remote debugging.

To avoid accidentally overwriting some files of the toolchain, remove write
permission with

\begin{lstlisting}[style=inline]
% sudo chown -R root:wheel /opt/OSELAS.Toolchain-2011.11.0
\end{lstlisting}

\chapter{Building an Embedded Linux project}

\section{Using OSELAS.BSP-Pengutronix-Generic}
\label{sec:genericbsp}

The OSELAS.BSP-Pengutronix-Generic BSP from Pengutronix is a good start since it
can be used to try out \emph{PTXdist} without real hardware\footnote{except from
the computer on which \emph{PTXdist} runs, of course}. In addition, it
doesn't need parts of \emph{PTXdist} that currently don't work on \macos{}, see
section \ref{sec:limitations}.

\subsection{Getting the BSP}

Since the last release is from November 2011, I created a public GIT
repository which contains a more up-to-date version of the BSP. Apart from the
project migration to the new version, only minor fixes were made and the
documentation \cite{PTXdistBSP} still fully applies.

To retrieve my forked version of the BSP, use

\begin{lstlisting}[style=inline]
% git clone https://bitbucket.org/bwalle/oselas.bsp-pengutronix-generic.git
% git checkout -b macos origin/macos
\end{lstlisting}

Select the arm variant:

\begin{lstlisting}[style=inline]
% ptxdist platform configs/arm-qemu-2011.11.0/platformconfig
\end{lstlisting}

and build the full image with

\begin{lstlisting}[style=inline]
% ptxdist images
\end{lstlisting}

After that, the directory \textsf{platform-versatilepb/images} should contain
following files:

\begin{description}
  \item[\textsf{kernelimage}] the Linux kernel
  \item[\textsf{root.ext2}] the ext2 root file system
  \item[\textsf{hd.img}] a disk image that contains a partition table in
    addition to the file system
\end{description}

\subsection{Building FSF GCC}

Before building Qemu we have to download another toolchain. \macos{} ships with
LLVM GCC (\url{http://www.llvm.org}). However, to successfully \emph{run} Qemu
we need FSF\footnote{Free Software Foundation} GCC which is basically the
``real'' GCC as you know it from Linux. One of the reasons why Apple switched
from FSF GCC to LLVM is licensing. Apple wants to avoid GPL in version~3.

If you have an older \macos{} installation that has been updated from time to
time, you may have already an FSF GCC available as \textsf{gcc-4.2}. Otherwise,
\emph{MacPorts} makes it easy to install the last FSF GCC that Apple provided:

\begin{lstlisting}[style=inline]
% sudo port install apple-gcc42
\end{lstlisting}

\subsection{Building Qemu}

Unfortunately, the \emph{MacPorts} version of Qemu for ARM doesn't work, at
least it didn't in my tests. However, the
current\footnote{7914cb3c738a03a5d5f7cb32c3768bc62eb1e944} GIT version does.
So let's just build our own Qemu.

Now retrieve the Qemu GIT tree with:

\begin{lstlisting}[style=inline]
% git clone git://git.qemu.org/qemu.git
% cd qemu
\end{lstlisting}

We need one patch that is not (yet) mainline to apply. The patch is from
\emph{MacPorts,} but for convenience I added it to the BSP git repo. To apply
it, just use

\begin{lstlisting}[style=inline]
% git clone git://git.qemu.org/qemu.git
% patch -p0 -i \
   <path-to-bsp>/configs/arm-qemu-2011.11.0/patches ...
    .../patch-cocoa-uint16-redefined.diff
\end{lstlisting}

Configure and build Qemu\footnote{You may replace \texttt{gcc-apple-4.2} by
\texttt{gcc-4.2} if you didn't use \emph{MacPorts} to build that GCC}:

\begin{lstlisting}[style=inline]
% ./configure --cc=gcc-apple-4.2 --host-cc=gcc-apple-4.2 \
  --target-list=arm-softmmu
% make -j$[2 * $(sysctl -n hw.ncpu)]
\end{lstlisting}

After a while, the file \textsf{./arm-softmmu/qemu-system-arm} should be ready.

\subsection{Running the System}

Change back to the BSP directory and execute

\begin{lstlisting}[style=inline]
PATH=../qemu/arm-softmmu/:$PATH configs/arm-qemu-2011.11.0/run
\end{lstlisting}

If you have another directory layout, adjust the \texttt{\$PATH} setting.
After a while, you should see the login prompt as shown in figure
\ref{fig:qemu_arm}. You should be able to login as \texttt{root} without
password.

\begin{figure}[htb]
  \begin{center}
    \includegraphics[width=12cm]{qemu_arm.png}
  \end{center}
  \caption{Qemu running an ARM kernel}
  \label{fig:qemu_arm}
\end{figure}

Now open another Terminal window and execute \texttt{telnet localhost 4444}.
Even there you should be able to login as \texttt{root}. The Qemu network stack
has been setup to forward the port 4444 the host to port 23 of the emulated
system. Just look in the file \textsf{configs/arm-qemu-2011.11.0/run} to see how
this was made.

\section{Using real Hardware}

Embedded Linux development is boring without hardware toys. You should be able
to use \emph{any} PTXdist-based BSP with \macos{} as long as you don't need
something that is listed in section~\ref{sec:limitations}.

If you have a \emph{Beagleboard} (\url{http://beagleboard.org/}) or a
\emph{BeagleBone} (\url{http://beagleboard.org/bone}), my BSP at
\url{https://bitbucket.org/bwalle/ptxdist-arm-boards} may be a good start.

\chapter{Limitations}
\label{sec:limitations}

This section should list was currently is known to not work.

\section{Building UBI or JFFS2 images}

Currently the \emph{host-mtd-utils} don't compile. I already have something
that works ready at \url{https://bitbucket.org/bwalle/mtd-utils}, but it's not yet
integrated.

\section{Linux kernel}

Not every Linux kernel version can be compiled without patches. However, kernel
3.4 at least on ARM (tested OMAP3, Marvell Kirkwood and the ``versatilepb''
platform used in section \ref{sec:genericbsp}) works.

Some patches you may need\footnote{all GIT hashes are for
\url{git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux-2.6.git}}:

\begin{itemize}
  \item \texttt{scripts/Kbuild.include: Fix portability problem of "echo -e"}
    \newline (875de98623fa2b29f0cb19915fe3292ab6daa1cb)
  \item \texttt{ARM: 7184/1: fix \$(CROSS\_COMPILE) prefix missing from size invocation}
    \newline (1ec332a3756a22405d2fbd5352e3afab556cb205)
\end{itemize}

\section{Bootloader}

As far as I know, Barebox (\url{http://www.barebox.org}) currently doesn't
build while U-Boot (\url{http://www.denx.de/wiki/U-Boot}) works. Some U-Boot
developers occasionally run \macos{} themselves.

\chapter{Using a Mac for Embedded Linux Development}

This section doesn't have anything to do with \emph{PTXdist} in particular.
Instead, it collects some hints that makes Linux development easier with a Mac.

\section{Using a serial console}

Macs don't have a RS-232 port, but PC Laptops also don't have one. Most
USB-to-RS-232 adapters also work on \macos{}. The interfaces are named
\textsf{/dev/tty.\emph{name}}, for example
\textsf{/dev/tty.usbserial-TIV90V1YA} or just \textsf{/dev/tty.usbserial}.

Most console-based Terminal programs can be also used on \macos{}.
\emph{screen(1)} is even shipped by default, but I prefer \emph{picocom(1)}
which can be installed using \emph{MacPorts} and used like

\begin{lstlisting}[style=inline]
picocom -b 115200 /dev/tty.usbserial-TIV90V1YA
\end{lstlisting}

To quit it, use \textsf{C-a C-q}. To send a break character which is important
for using \emph{Sysrq} \cite{KernelSysrq}, use \textsf{C-a C-\textbackslash{}} followed by the
key that should sent to the kernel. If you can't remember, use \textsf{h} for
\emph{help}.

\section{Exporting directories via NFS}

For application development exporting the root file system on NFS rather than
copying it to the target again and again, improves the turnaround speed a lot.
Using \macos{} is surprisingly easy.

Just create a file \textsf{/etc/exports} (if it doesn't already exist)
containling a line such as

\begin{lstlisting}[style=inline]
/Volumes/Daten/devel -maproot=0 -network 192.168.0.0 -mask 255.255.255.0
\end{lstlisting}

The option \emph{-maproot=0} is basically the same as \emph{no\_root\_squash} on
Linux. You can even map other users, see the manpage \emph{exports(5)} for
details.

Creating the file --- at least if you get it initially right --- should be
sufficient. The NFS server automatically detects creation or modification of
that file. However, one just can use \texttt{nfsd status} to check the status
of the NFS server and \texttt{nfsd checkexports} to check the syntax after
editing \textsf{/etc/exports}. Read \emph{nfsd(8)} for more details.

Like on Linux, the \emph{showmount(8)} command shows what you're exporting:

\begin{lstlisting}[style=inline]
% sudo showmount -e
Exports list on localhost:
/Volumes/Daten/devel                192.168.0.0
\end{lstlisting}

On the target side everything stays the same.


\newpage

\section{Mounting \emph{ext2} partitions}

When working with root file systems on SD cards it's quite useful if you
cannot only copy images but also look at the files. Especially as most Macs
have a builtin SD card reader.

While \macos{} has no kernel driver for \emph{ext2}, like Linux \macos{} supports
file systems in userspace. Just install \emph{ext2fuse} with

\begin{lstlisting}[style=inline]
% sudo port install ext2fuse
\end{lstlisting}

and then you're able to mount an \emph{ext2} or \texttt{ext3} partition using

\begin{lstlisting}[style=inline]
% diskutil list # find out the name of the disk
% ext2fuse /dev/disk1s2 ~/mnt
\end{lstlisting}

The important thing is that \emph{ext2fuse} has to be executed as the same
user as the one that works with the file. So the mountpoint must be accessible
as the user --- that's why I'm not using a directory below \textsf{/Volumes}.

\begin{appendix}
\chapter{About this Document}

This document has been typeset with \LaTeX{}. The sources of the document can be
downloaded at \url{https://bitbucket.org/bwalle/ptxdist-macos-doc}.

The canonical location for the PDF is
\url{http://bwalle.de/docs/ptxdist\_mac.pdf}. Please send any feedback to me via
email \url{bernhard@bwalle.de}.

\chapter{Change History}

\begin{tabular}{|lp{120mm}|}
  \hline
  \textbf{Date}  & \textbf{Modification} \\
  \hline
  \hline
  2012-04-21     & Add hint to disable \emph{Spotlight} indexing. \\
  \hline
  2012-04-20     & Add description about case insensitive file system. \\
                 & Add \emph{dialog} as optional dependency. \\
  \hline
\end{tabular}


\end{appendix}

\begin{thebibliography}{99}
  \bibitem{PTXdistBSP} \textbf{How to become a PTXdist Guru}, \newline
    \url{http://www.ptxdist.org/software/ptxdist/appnotes/OSELAS.BSP-Pengutronix-Generic-arm-Quickstart.pdf}

  \bibitem{PTXdistInstallation} \textbf{Installing PTXdist}, \newline
    \url{http://www.ptxdist.org/software/ptxdist/appnotes/AppNote_InstallingPtxdist.pdf}

  \bibitem{MacPortsGuide} \textbf{MacPorts Guide}, \newline
    \textsc{Mark Duling}, \textsc{Dr. Michael A Maibaum}, \textsc{Will Barton}, \newline
    \url{http://guide.macports.org/}

  \bibitem{KernelSysrq} \textbf{Linux Magic System Request Key Hacks} \newline
    \url{http://kernel.org/doc/Documentation/sysrq.txt}

\end{thebibliography}

\end{document}

% vim: set spell spelllang=en_gb:
